# build-artifacts

Dieses Projekt zeigt wie man mit GitLab build Artefakte erzeugt und über die Benutzeroberfläche verfügbar macht.

Für build Artefakte sollte darauf geachtet werden, dass `expire_in` in den entsprechenden Jobs angegeben wird, damit Artefakte nicht bis in alle Zeit gespeichert werden müssen. Das kann je nach Datenmenge und Storage Provider teuer sein. (Siehe auch [artifacts:expire_in](https://docs.gitlab.com/ee/ci/yaml/#artifactsexpire_in))

Nach einem erfolgreichen Pipeline-Durchlauf finden sich die Artefakte in `CI / CD` -> `Pipelines` rechts an der jeweiligen Pipeline.
